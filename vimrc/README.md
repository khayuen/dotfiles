Requirements
------------
1. [Vundle](https://github.com/VundleVim/Vundle.vim)
2. [The Silver Searcher](https://github.com/ggreer/the_silver_searcher)

Install
-------
1. `ln -sf ~/<directory>/dotfiles/vimrc/.vimrc ~/.vimrc`
2. `vim +PluginInstall +qall`
