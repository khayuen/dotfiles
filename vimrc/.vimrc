set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Plugins " {{{
Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-surround'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
Plugin 'kien/ctrlp.vim'
Plugin 'rking/ag.vim'
Plugin 'othree/yajs.vim'
Plugin 'chriskempson/vim-tomorrow-theme'

" " }}}

call vundle#end()
filetype plugin indent on

let mapleader=","
colorscheme Tomorrow-Night

imap jk <ESC>

set modelines=0
set number
set smartindent
set shiftwidth=2
set cursorline
set expandtab
set tabstop=2
set ignorecase
set smartcase
set hlsearch
set nowrap
set t_Co=256
set laststatus=2
set nobackup
set nowritebackup
set noswapfile 

syntax enable

" NERDTree
nmap <leader>n :NERDTreeToggle<CR>

" CtrlP
let g:ctrlp_custom_ignore = '\v[\/](\.(git|hg|svn)|node_modules)$'
let g:ctrlp_working_path_mode = 1

" vim-airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" Tab between buffers
noremap <tab> <c-w><c-w>

" Switch between last two buffers
nnoremap <leader><leader> <C-^>

" Open new buffers
nmap <leader>s<left>   :leftabove  vnew<cr>
nmap <leader>s<right>  :rightbelow vnew<cr>
nmap <leader>s<up>     :leftabove  new<cr>
nmap <leader>s<down>   :rightbelow new<cr>
